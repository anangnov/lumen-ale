<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ClassSchedule;
use DB;

class ClassController extends Controller {
    public function showAll(Request $request) {
        try {
            $data = DB::table('class_mentor_student')
                        ->leftJoin('class_schedule', 'class_mentor_student.class_schedule_id', '=', 'class_schedule.id')
                        ->leftJoin('mentor', 'class_mentor_student.mentor_id', '=', 'mentor.id')
                        ->leftJoin('student', 'class_mentor_student.student_id', '=', 'student.id')
                        ->select('class_mentor_student.*', 'class_schedule.name', 'class_schedule.deskripsi',
                            'class_schedule.jadwal_tgl_mulai', 'class_schedule.jadwal_tgl_akhir', 
                            'class_schedule.jadwal_waktu_mulai', 'class_schedule.jadwal_waktu_akhir',
                            DB::raw('CONCAT(mentor.firstname, " ", mentor.lastname) as mentor_name'), 
                            DB::raw('CONCAT(student.firstname, " ", student.lastname) as student_name'))
                        ->skip($request->offset)
                        ->take($request->limit);

            if ($request->status == null) {
                $response = $data->get();
            } else {
                $response = $data->where('class_schedule.status', '=', $request->status)->get();
            }
        } catch (Exception $ex) {
            return response_helper('ERR', '', $ex->getMessage());
        }

        return response_helper('FOUND', $response, '');
    }

    public function showDetail(Request $request) {
        try {
            $data = DB::table('class_mentor_student')
                        ->leftJoin('class_schedule', 'class_mentor_student.class_schedule_id', '=', 'class_schedule.id')
                        ->where('class_mentor_student.id', '=', $request->id)
                        ->first();
        } catch (Exception $ex) {
            return response_helper('ERR', '', $ex->getMessage());
        }

        return response_helper($data == null ? 'NOT_FOUND' : 'FOUND', $data, '');
    }
}