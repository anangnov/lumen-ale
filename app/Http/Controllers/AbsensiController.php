<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Absent;
use App\User;

class AbsensiController extends Controller {
    public function show(Request $request) {
        $this->validate($request, [
            'offset' => 'required',
            'limit' => 'required'
        ]);

        try {
            $data = Absent::skip($request->offset)->take($request->limit)->get();
        } catch (Exception $ex) {
            return response_helper('ERR', '', '');
        }

        return response_helper('FOUND', $data, '');
    }

    public function showDetail(Request $request) {
        try {
            $data = Absent::find($request->absent_id);

            if ($data == null) {
                return response_helper('NOT_FOUND', '', 'Absent Id '.$request->absent_id.' tidak ditemukan');
            }
        } catch (Exception $ex) {
            return response_helper('ERR', '', '');
        }

        return response_helper($data == null ? 'NOT_FOUND' : 'FOUND', $data, '');
    }

    public function absent(Request $request) {
        $this->validate($request, [
            'user_id' => 'required',
            'tgl_absen' => 'required',
            'jam_absen' => 'required'
        ]);

        try {
            $user = User::where('id', '=', $request->user_id)->first();

            if ($user == null) {
                return response_helper('NOT_FOUND', '', 'User id '.$request->id.' tidak ditemukan');
            }

            if ($user->role_id !== 3) {
                return response_helper('ERR', '', 'Anda bukan student.');
            }

            $check = Absent::where('user_id', '=', $request->user_id)->where('role_id', '=', $user->role_id)->get()->last();
            $date = date('Y-m-d');
            
            if ($check->tgl_absen == $date) {
                return response_helper('ERR', '', 'Anda sudah absen hari ini.');
            }

            $data = [
                'user_id' => $request->user_id,
                'role_id' => $user->role_id,
                'tgl_absen' => $request->tgl_absen,
                'jam_absen' => $request->jam_absen
            ];

            Absent::create($data);
        } catch (Exception $ex) {
            return response_helper('ERR', '', '');
        }

        return response_helper('INSERT', $data, '');
    }
}