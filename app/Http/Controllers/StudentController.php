<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Student;
use App\Model\Mentor;
use App\Model\ClassMentorStudent;
use App\Model\ClassSchedule;
use DB;

class StudentController extends Controller {
    public function show(Request $request) {
        $this->validate($request, [
            'offset' => 'required',
            'limit' => 'required'
        ]);

        try {
            $data = Student::skip($request->offset)->take($request->limit)->get();
        } catch (Exception $e) {
            return response_helper('ERR', '', $ex->getMessage());
        }

        return response_helper('FOUND', $data, '');
    }

    public function updateProfile (Request $request) {
        $this->validate($request, [
			'foto_profile' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1048'
        ]);

        try {
            // Upload file to path
            $path = public_path('student');
            
            if ($request->has('foto_profile')) {
                $file = $request->file('foto_profile');
                $file->move($path, time().'_'.$file->getClientOriginalName());
                $file_name = 'student/'.time().'_'.$file->getClientOriginalName();
            } else {
                $file_name = $request->file('foto_profile');
            }
            // End Upload file to path
        
            $data = [
                'alamat' => $request->alamat,
                'no_telp' => $request->no_telp,
                'foto_profile' => $file_name
            ];

            $find = Student::find($request->id);
            
            // Validation id
            if ($find == null) {
                return response_helper('NOT_FOUND', '', 'Id '.$request->id.' tidak ditemukan');
            }
            // End Validation id

            $find->update(array_filter($data));
        } catch (Exception $ex) {
            return response_helper('ERR', '', $ex->getMessage());
        }

        return response_helper('UPDATE', $data, '');
    }

    public function joinClass(Request $request) {
        try {
            $data = [
                'mentor_id' => $request->mentor_id,
                'student_id' => $request->student_id,
                'class_schedule_id' => $request->class_schedule_id
            ];

            $checkMentor = Mentor::where('id', '=', $request->mentor_id)->first();
            $checkStudent = Student::where('id', '=', $request->student_id)->first();
            $checkClassSchedule = ClassSchedule::where('id', '=', $request->class_schedule_id)->first();
            $checkExist = ClassMentorStudent::where('mentor_id', '=', $request->mentor_id)
                            ->where('student_id', '=', $request->student_id)
                            ->where('class_schedule_id', '=', $request->class_schedule_id)
                            ->where('status', '=', 1)
                            ->first();

            if ($checkMentor == null) {
                return response_helper('NOT_FOUND', '', 'Mentor Id '.$request->mentor_id.' tidak ditemukan');
            }

            if ($checkStudent == null) {
                return response_helper('NOT_FOUND', '', 'Student Id '.$request->student_id.' tidak ditemukan');
            }

            if ($checkClassSchedule == null) {
                return response_helper('NOT_FOUND', '', 'Class Schedule Id '.$request->class_schedule_id.' tidak ditemukan');
            }

            if ($checkExist !== null) {
                return response_helper('FOUND', '', 'Anda telah terdaftar dalam kelas ini');
            }

            ClassMentorStudent::create($data);
        } catch (Exception $ex) {
            return response_helper('ERR', '', $ex->getMessage());
        }

        return response_helper('INSERT', $data, 'Success join class');
    }

    public function showDetailStudent(Request $request) {
        $this->validate($request, [
            'student_id' => 'required'
        ]);

        try {
            $data = Student::find($request->student_id);

            if ($data == null) {
                return response_helper('NOT_FOUND', '', 'Student Id '.$request->student_id.' tidak ditemukan');
            }
        } catch (Exception $e) {
            return response_helper('ERR', '', $ex->getMessage());
        }

        return response_helper('FOUND', $data, '');
    }

    public function classScheduleMateriDetail(Request $request) {
        try {
            $data = DB::table('class_mentor_student')
                        ->leftJoin('class_schedule', 'class_mentor_student.class_schedule_id', '=', 'class_schedule.id')
                        ->leftJoin('mentor', 'class_mentor_student.mentor_id', '=', 'mentor.id')
                        ->leftJoin('student', 'class_mentor_student.student_id', '=', 'student.id')
                        ->select('class_mentor_student.*', 'class_schedule.name', 'class_schedule.deskripsi',
                            'class_schedule.jadwal_tgl_mulai', 'class_schedule.jadwal_tgl_akhir', 
                            'class_schedule.jadwal_waktu_mulai', 'class_schedule.jadwal_waktu_akhir',
                            'student.id as student_id', 'student.user_id as user_id',
                            DB::raw('CONCAT(mentor.firstname, " ", mentor.lastname) as mentor_name'), 
                            DB::raw('CONCAT(student.firstname, " ", student.lastname) as student_name'))
                        ->where('student_id', '=', $request->student_id);

            $response = $data->get();
        } catch (Exception $ex) {
            return response_helper('ERR', '', $ex->getMessage());
        }

        return response_helper('FOUND', $response, '');
    }
}