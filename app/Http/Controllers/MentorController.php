<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Mentor;
use App\Model\ClassSchedule;
use App\Model\Material;

class MentorController extends Controller 
{
    public function show(Request $request) {
        $this->validate($request, [
            'offset' => 'required',
            'limit' => 'required'
        ]);

        try {
            $data = Mentor::skip($request->offset)->take($request->limit)->get();
        } catch (Exception $ex) {
            return response_helper('ERR', '', $ex->getMessage());
        }

        return response_helper('FOUND', $data, '');
    }

    public function showDetail(Request $request) {
        $this->validate($request, [
            'mentor_id' => 'required'
        ]);

        try {
            $data = Mentor::find($request->mentor_id);

            if ($data == null) {
                return response_helper('NOT_FOUND', '', 'Mentor Id '.$request->mentor_id.' tidak ditemukan');
            }
        } catch (Exception $ex) {
            return response_helper('ERR', '', $ex->getMessage());
        }

        return response_helper('FOUND', $data, '');
    }

    public function createClassSchedule(Request $request) {
        $this->validate($request, [
            'mentor_id' => 'required',
            'name' => 'required',
            'deskripsi' => 'required',
            'jadwal_tgl_mulai' => 'required',
            'jadwal_tgl_akhir' => 'required',
            'jadwal_waktu_mulai' => 'required',
            'jadwal_waktu_akhir' => 'required'
        ]); 

        try {
            // Check Mentor 
            $check = Mentor::where('id', '=', $request->mentor_id)->first();

            if ($check == null) {
                return response_helper('NOT_FOUND', '', 'Mentor id '.$request->mentor_id.' tidak ditemukan');
            }
            // End Check Mentor

            $data = [
                'mentor_id' => $request->mentor_id,
                'name' => $request->name,
                'deskripsi' => $request->deskripsi,
                'jadwal_tgl_mulai' => $request->jadwal_tgl_mulai,
                'jadwal_tgl_akhir' => $request->jadwal_tgl_akhir,
                'jadwal_waktu_mulai' => $request->jadwal_waktu_mulai,
                'jadwal_waktu_akhir' => $request->jadwal_waktu_akhir
            ];

            // Create Class Schedule
            ClassSchedule::create($data);
            // End Create Class Schedule
        } catch (Exception $ex) {
            return response_helper('ERR', '', $ex->getMessage());
        }

        return response_helper('INSERT', $data, '');
    }

    public function updateClassSchedule(Request $request) {
        try {
            // Check Mentor 
            $checkClass = ClassSchedule::where('id', '=', $request->id)->first();
            $checkMentor = Mentor::where('id', '=', $request->mentor_id)->first();

            if ($checkClass == null) {
                return response_helper('NOT_FOUND', '', 'Id '.$request->id.' tidak ditemukan');
            }

            if ($checkMentor == null) {
                return response_helper('NOT_FOUND', '', 'Mentor Id '.$request->id.' tidak ditemukan');
            }
            // End Check Mentor

            $data = [
                'id' => $request->id,
                'mentor_id' => $request->mentor_id,
                'name' => $request->name,
                'deskripsi' => $request->deskripsi,
                'jadwal_tgl_mulai' => $request->jadwal_tgl_mulai,
                'jadwal_tgl_akhir' => $request->jadwal_tgl_akhir,
                'jadwal_waktu_mulai' => $request->jadwal_waktu_mulai,
                'jadwal_waktu_akhir' => $request->jadwal_waktu_akhir
            ];

            // Update Class Schedule
            $find = ClassSchedule::find($request->id);
            $find->update(array_filter($data));
            // End Update Class Schedule
        } catch (Exception $ex) {
            return response_helper('ERR', '', '');
        }

        return response_helper('UPDATE', $data, '');
    }

    public function deactiveClassSchedule(Request $request) {
        try {
            $find = ClassSchedule::find($request->id);

            if ($find == null) {
                return response_helper('NOT_FOUND', '', 'Class Schedule Id '.$request->id.' tidak ditemukan');
            }
            
            $find->update([
                'status' => 2
            ]);
            
            Material::where('class_schedule_id', '=', $request->id)->update([ 'status' => 2 ]);
        } catch (Exception $ex) {
            return response_helper('ERR', '', $ex->getMessage());
        }

        return response_helper('UPDATE', '', 'Deactive Class Schedule');
    }

    public function createMateri(Request $request) {
        $this->validate($request, [
            'class_schedule_id' => 'required',
            'name' => 'required',
            'file_materi' => 'required|max:2048',
            'deskripsi' => 'required'
        ]);

        try {
            // Check Class Schedule
            $find = ClassSchedule::find($request->class_schedule_id);

            if ($find == null) {
                return response_helper('NOT_FOUND', '', 'Class Schedule Id '.$request->class_schedule_id.' tidak ditemukan');
            }
            // End Check Class Schedule

            // Upload file to path
            $path = public_path('mentor');
            
            if ($request->has('file_materi')) {
                $file = $request->file('file_materi');
                $file->move($path, time().'_'.$file->getClientOriginalName());
                $file_name = 'mentor/'.time().'_'.$file->getClientOriginalName();
            } else {
                $file_name = $request->file('mentor');
            }
            // End Upload file to path

            $data = [
                'class_schedule_id' => $request->class_schedule_id,
                'name' => $request->name,
                'file_materi' => $file_name,
                'video_materi' => $request->video_materi, // Link embed, etc = youtube
                'deskripsi' => $request->deskripsi
            ];

            Material::create($data);
        } catch (Exception $ex) {
            return response_helper('ERR', '', $ex->getMessage());
        }

        return response_helper('INSERT', $data, '');
    }

    public function classScheduleSelfList(Request $request) {
        $this->validate($request, [
            'mentor_id' => 'required',
            'offset' => 'required',
            'limit' => 'required'
        ]);

        try {
            // Check Mentor Id
            $find = Mentor::find($request->mentor_id);

            if ($find == null) {
                return response_helper('NOT_FOUND', '', 'Mentor Id '.$request->mentor_id.' tidak ditemukan');
            }
            // End Check Class Schedule

            $data = ClassSchedule::where('mentor_id', '=', $request->mentor_id)->skip($request->offset)->take($request->limit)->get();
        } catch (Exception $ex) {
            return response_helper('ERR', '', $ex->getMessage());
        }

        return response_helper('FOUND', $data, '');
    }

    public function updateProfileMentor(Request $request) {
        $this->validate($request, [
			'foto_profile' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1048'
        ]);

        try {
            // Upload file to path
            $path = public_path('mentor');
            
            if ($request->has('foto_profile')) {
                $file = $request->file('foto_profile');
                $file->move($path, time().'_'.$file->getClientOriginalName());
                $file_name = 'mentor/'.time().'_'.$file->getClientOriginalName();
            } else {
                $file_name = $request->file('foto_profile');
            }
            // End Upload file to path
        
            $data = [
                'alamat' => $request->alamat,
                'no_telp' => $request->no_telp,
                'foto_profile' => $file_name,
                'pendidikan' => $request->pendidikan
            ];

            $find = Mentor::find($request->id);
            
            // Validation id
            if ($find == null) {
                return response_helper('NOT_FOUND', '', 'Id '.$request->id.' tidak ditemukan');
            }
            // End Validation id

            $find->update(array_filter($data));
        } catch (Exception $ex) {
            return response_helper('ERR', '', $ex->getMessage());
        }

        return response_helper('UPDATE', $data, '');
    }
}