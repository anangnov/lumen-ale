<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Model\Student;
use App\Model\Mentor;
use App\Helpers\EncryptionHelper;
use Illuminate\Support\Facades\Mail;
use Auth;
use DB;

class AuthController extends Controller {
    public function register(Request $request) {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'role_id' => 'required'
        ]); 

        // Validate Role
        $role = [2, 3];

        if (!in_array($request->role_id, $role)) {
            return response()->json([
                'error' => true,
                'message' => 'Role expected 2 or 3.'
            ], 401);
        }
        
        // Remove space and lowercase username
        $space = preg_replace('/\s*/', '', $request->firstname. '' .$request->lastname);
        $lower = strtolower($space);
        $username = $lower;
        $bcrypt = new EncryptionHelper();
        $password = $bcrypt->bcrypt($request->password);
        $user = User::create([
            'username' => $username,
            'email' => $request->email,
            'password' => $password,
            'role_id' => $request->role_id
        ]);

        $maxId = DB::table('users')->max('id');

        if ($request->role_id == 2) {
            Mentor::create([
                'user_id' => $maxId,
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
            ]);
        }

        if ($request->role_id == 3) {
            Student::create([
                'user_id' => $maxId,
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
            ]);
        }
        
        $roleReq = $request->role_id;
        $valReq = '';

        if ($roleReq == 2) {
            $valReq = 'mentor';
        } else if ($roleReq == 3) {
            $valReq = 'student';

            // Mail Verification for Student
            $dataMail = [
                'email' => $request->email,
                'base_url' => env('BASE_URL'),
                'encrypt_email' => cryption($request->email, 'encrypt')
            ];

            Mail::send('verification-mail', $dataMail, function($message) use($request, $username) {
                $message->to($request->email, $username)->subject('Email Verification From Kelas Kita');
                $message->from(env('MAIL_FROM'), 'Kelas Kita Support');
            });
            // End Mail Verification for Student
        }

        return response()->json([
            'error' => false,
            'status' => 200,
            'message' => $valReq == 'student' ?
                'Success register as ' . $valReq . ', Please check your email for verification.' 
                : 'Success register as ' . $valReq . ', Please login.',
        ], 200);
    }

    public function login(Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        
        $user = User::where('email', '=', $request->email)->first();
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if ($user->role_id == 3) {
            if ($user->status == 1) {
                return response()->json([
                    'error' => true,
                    'status' => 400,
                    'message' => 'Account anda belum terverifikasi, silahkan cek email untuk melakukan verifikasi.'
                ], 401);
            }
        }
        
        if ($user && Hash::check(($request->password), $user->password)) {
            $token = $user->createToken('joss')->accessToken;

            return response()->json([
                'error' => false,
                'status' => 200,
                'message' => 'Success login.',
                'role_id' => $user->role_id,
                'username' => $user->username,
                'token' => $token
            ], 200);
        } else {
            return response()->json([
                'error' => true,
                'status' => 401,
                'message' => 'Username or password is wrong'
            ], 401);
        }
    }
}