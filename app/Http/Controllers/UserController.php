<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Helpers\EncryptionHelper;

class UserController extends Controller 
{
    public function verifyMAil() {
        $email = $_GET['email'];
        $encryptParams = $_GET['param_name'];
        $date = date("Y-m-d H:i:s");

        $user = User::where('email', '=', $email)->first();
        $user->update([
            'status' => 1,
            'email_verified_at' => $date
        ]);

        return redirect()->to(env('BASE_URL') . '/info/verified/' . $encryptParams);
    }

    public function infoAccount($params_name) {
        $decryptMail = cryption($params_name, 'decrypt');
        $email = '';
        
        if ($decryptMail == true) {
            $email = $decryptMail;
        }
        
        $user = User::where('email', '=', $email)->first();

        if ($user == null) {
            abort(404);
        }

        return view('alert-info');
    }

    public function sendForgotPassword(Request $request) {
        $user = User::where('email', '=', $request->email)->first();

        if ($user == null) {
            return response_helper('NOT_FOUND', '', 'Email tidak ditemukan.');
        }

        $dataMail = [
            'email' => $request->email,
            'username' => $user->username,
            'params' => cryption($request->email, 'encrypt'),
            'base_url' => env('BASE_URL_DEV')
        ];

        Mail::send('forgot-password', $dataMail, function($message) use($request, $user) {
            $message->to($request->email, $user->username)->subject('Forgot Password From Kelas Kita');
            $message->from(env('MAIL_FROM'), 'Kelas Kita Support');
        });

        return response_helper('SUCCESS', '', 'Cek email anda.');
    }
 
    public function forgotPassword($params_name) {
        $decryptMail = cryption($params_name, 'decrypt');
        $email = '';
        
        if ($decryptMail == true) {
            $email = $decryptMail;
        }
        
        $user = User::where('email', '=', $email)->first();

        if ($user == null) {
            abort(404);
        }

        return view('forgot-password-form');
    }

    public function resetPassword($params_name, Request $request) {
        $this->validate($request, [
            'password' => 'required|confirmed'
        ]);

        $decryptMail = cryption($params_name, 'decrypt');
        $email = '';
        
        if ($decryptMail == true) {
            $email = $decryptMail;
        }

        $user = User::where('email', '=', $email)->first();

        if($user == null) {
            return response_helper('ERR', '', 'User tidak valid.');
        }

        $en = new EncryptionHelper();
        $pass = $en->bcrypt($request->password);

        $user->update([
            'password' => $pass
        ]);

        return response_helper('SUCCESS', '', 'Success update password.');
    }
}