<?php

use App\Helpers\ResponseHelper;
use App\Helpers\EncryptionHelper;

if (!function_exists('public_path')) {
    /**
     * Public path
     *
     * @param  string $path Filename
     * @return string
     */
    function public_path($path = null) {
        return rtrim(app()->basePath('public/'. $path), '/');
    } 
}

if (!function_exists('response_helper')) {
    /**
     * Response helper
     *
     * @param  string $message Message
     * @param  string $data Data
     * @param  string $info Info
     * @return json
     */
    function response_helper($message, $data, $info) {
        return new ResponseHelper($message, $data, $info);
    }
}

if (!function_exists('cryption')) {
    /**
     * Crypt
     *
     * @param  string $value Value
     * @param  string $action Action
     * @return string
     */
    function cryption($value, $action) {
        $enc = new EncryptionHelper();
        $cryption = $enc->crypt($value, $action);
        
        return $cryption;
    }
}