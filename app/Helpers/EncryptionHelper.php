<?php

namespace App\Helpers;

class EncryptionHelper {
    public function bcrypt($value, $options = []) {
        return app('hash')->make($value, $options);
    }

    public function crypt($value, $action) {
        $secretKey = 'w2JjhmQanescNLrxUFVacCImeos3H5wP';
        $secretClient = 'EJv2ISXuflVZyF03k4JpyevNONA2tE9K';
        $result = false;
        $encryptMethod = 'AES-256-CBC';
        $key = hash('sha256', $secretKey);
        $client = substr(hash('sha256', $secretKey), 0, 16);

        if ($action == 'encrypt') {
            $result = base64_encode(openssl_encrypt($value, $encryptMethod, $key, 0, $client));
        } else if ($action == 'decrypt') {
            $result = openssl_decrypt(base64_decode($value), $encryptMethod, $key, 0, $client);
        }

        return $result;
    }
}