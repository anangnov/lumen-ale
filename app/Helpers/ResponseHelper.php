<?php

namespace App\Helpers;

use Nahid\JsonQ\Jsonq;
use JsonSerializable;

class ResponseHelper implements JsonSerializable {
    private $name;
    private $message;
    private $status;
    private $data;

    public function __construct($name, $data, $info) {
        $this->name = $name;
        $this->data = $data;
        $this->info = $info;
    } 

    public function getName() {
        return $this->name;
    }

    public function getData() {
        return $this->data;
    }

    public function getInfo() {
        return $this->info;
    }

    public function jsonSerialize() {
        $path = storage_path() . "/json/message.json";
        $json = new Jsonq($path);
        $cli = $json->where('name', '=', $this->name);
        $response = $cli->get();

        $error = $response[$this->name]['error'];
        $name = $response[$this->name]['name'];
        $message = $response[$this->name]['message'];
        $status = $response[$this->name]['status'];

        return [
            'error' => $error,
            'name' => $name,
            'message' => $message,
            'status' => $status,
            'info' => $this->info,
            'data' => $this->data
        ];
    }
}