<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClassMentorStudent extends Model
{
    protected $table = 'class_mentor_student';

    protected $fillable = [
        'student_id', 'mentor_id',
        'class_schedule_id'
    ];
}
