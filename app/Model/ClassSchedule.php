<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClassSchedule extends Model
{
    protected $table = 'class_schedule';

    protected $fillable = [
        'mentor_id', 'name', 'deskripsi', 
        'jadwal_tgl_mulai', 'jadwal_tgl_akhir',
        'jadwal_waktu_mulai', 'jadwal_waktu_akhir',
        'status'
    ];
}
