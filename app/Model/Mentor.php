<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Mentor extends Model
{
    protected $table = 'mentor';
    
    protected $fillable = [
        'user_id', 'firstname', 'lastname',
        'no_telp', 'pendidikan', 'alamat', 'foto_profile'
    ];
}
