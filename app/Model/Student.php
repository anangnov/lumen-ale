<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'student';
    
    protected $fillable = [
        'user_id', 'firstname', 'lastname',
        'alamat', 'no_telp', 'foto_profile'
    ];
}
