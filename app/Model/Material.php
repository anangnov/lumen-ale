<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table = 'material';

    protected $fillable = [
        'class_schedule_id', 'name', 'video_materi',
        'file_materi', 'deskripsi', 'status'
    ];
}
