<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Absent extends Model
{
    protected $table = 'absent';

    protected $fillable = [
        'user_id', 'role_id', 
        'tgl_absen', 'jam_absen'
    ];
}
