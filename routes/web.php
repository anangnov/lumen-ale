<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Http\Middleware\MentorMiddleware;
use App\Http\Middleware\StudentController;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// Generate Key
$router->get('/api/v1/generate-key', function() {
    return \Illuminate\Support\Str::random(32);
});
// Generate Key

// Auth
$router->post('/api/v1/register', ['uses' => 'AuthController@register']);
$router->post('/api/v1/login', ['uses' => 'AuthController@login']);
// End Auth

$router->group(['prefix' => '/api/v1/admin', 'middleware' => 'auth'], function() use ($router) {
    $router->get('/student', ['uses' => 'StudentController@show']);
    $router->get('/student/detail', ['uses' => 'StudentController@showDetailStudent']);
    $router->get('/mentor', ['uses' => 'MentorController@show']);
    $router->get('/mentor/detail', ['uses' => 'MentorController@showDetail']);
    $router->post('/student/update', ['uses' => 'StudentController@updateProfile']);
    $router->get('/absensi', ['uses' => 'AbsensiController@show']);
    $router->get('/absensi/detail', ['uses' => 'AbsensiController@showDetail']);
    $router->get('/class', ['uses' => 'ClassController@showAll']);
    $router->get('/class/detail', ['uses' => 'ClassController@showDetail']);
});

$router->group(['prefix' => '/api/v1/student', 'middleware' => 'student'], function() use ($router) {
    $router->post('/update', ['uses' => 'StudentController@updateProfile']);
    $router->post('/absensi/create', ['uses' => 'AbsensiController@absent']);
    $router->post('/class/join', ['uses' => 'StudentController@joinClass']);
    $router->get('/detail', ['uses' => 'StudentController@showDetailStudent']);
    $router->get('/detail-class-schedule-materi', ['uses' => 'StudentController@classScheduleMateriDetail']);
});

$router->group(['prefix' => '/api/v1/mentor', 'middleware' => 'mentor'], function() use ($router) {
    $router->post('/materi/create', ['uses' => 'MentorController@createMateri']);
    $router->get('/class-schedule', ['uses' => 'MentorController@classScheduleSelfList']);
    $router->get('/detail', ['uses' => 'MentorController@showDetail']);
    $router->post('/class-schedule/create', ['uses' => 'MentorController@createClassSchedule']);
    $router->post('/class-schedule/update', ['uses' => 'MentorController@updateClassSchedule']);
    $router->post('/class-schedule/deactive', ['uses' => 'MentorController@deactiveClassSchedule']);
    $router->post('/update', ['uses' => 'MentorController@updateProfileMentor']);
});

// Mail Route
$router->get('/info/verified/{params_name}', ['uses' => 'UserController@infoAccount']);
$router->get('/verify', ['uses' => 'UserController@verifyMAil']);
$router->post('/api/v1/send/forgot-password', ['uses' => 'UserController@sendForgotPassword']);
$router->get('/forgot-password/{params_name}', ['uses' => 'UserController@forgotPassword']);
$router->post('/api/v1/reset-password/{params_name}', ['uses' => 'UserController@resetPassword']);