# ALE API With Lumen

This project from Alcatel-Lucent Enterprise (ALE). Backend API menggunakan Framework Lumen.

## # Installation
clone or download this project

    git clone https://gitlab.com/anangnov/lumen-ale.git

installing vendor
    
    composer install

Copy .env.example .env
    
    cp .env.example .env

generate key in a way run `php -S localhost:8000 -t public`, then hit endpoint `/generate-key`.

Copy the result generate-key to variable `APP_KEY` in .env

Run in local
    
    php -S localhost:8000 -t public   


Email: anangnov99@gmail.com